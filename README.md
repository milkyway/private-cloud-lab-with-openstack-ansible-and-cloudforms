<img src="images/redhat.png" style="width: 350px;" border=0/>

# Private cloud lab with OpenStack, Ansible, and CloudForms (**T02098**)
**Date**: May 2017<br>

### Presenters/Lab Developers:
- **Vagner Farias**, <<vfarias@redhat.com>>, Senior Specialist Solution Architect
- **Robert Calva** <<rcalvaga@redhat.com>>, Senior Specialist Solution Architect
- **Mauricio Santacruz** <<msantacruz@redhat.com>>, Senior Solution Architect
- **Luciano Scorsin** <<lscorsin@redhat.com>>, Solution Architect

## Overview and Prerequisites

In this hands-on lab, you'll learn how a combination of Red Hat OpenStack Platform, Red Hat CloudForms and Red Hat Ansible Tower can be used to deliver cloud capabilities for your on-premises environment. Going through a series of tasks, you'll understand the role of each of these components, which can be summarized as:

- **Red Hat OpenStack Platform**, manages the infrastructure layer, providing infrastructure as a Service (IaaS) capabilities.
- **Red Hat Ansible Tower**, provides automation capabilities, enabling tasks to be executed in a controlled way across the environment.
- **Red Hat CloudForms**, provides cloud management capabilities, such as service catalogs which allows deploying complete applications through the click of a button.

This lab is geared towards systems administrators, cloud administrators and operators, architects, and others working on infrastructure operations management who would like to understand how a private cloud based on Red Hat products looks like. The prerequisites for this lab include:

- Solid virtualization concepts.
- Linux skills gained from Red Hat Certified Engineer (RHCE) or equivalent system administration skills.

Previous knowledge on OpenStack and Ansible would be helpful, but not required.

## Lab environment

The environment you're going to use is hosted online and was previously deployed and configured, so that you can focus your attention on how to use the technology. Each of you will be given your own unique **GUID**, which you'll use to access your own instance of the lab environment.

The High Level Design of the environment is shown in the following diagram.

![architecture diagram](images/privatecloud_lab_architecture.png)

All the tasks in this hand-on lab will be executed through the graphical user interfaces provided by the Red Hat Products, although all of them also provide command line or application programming interfaces.

It is required that you follow the order of the labs as some of them depend on previous tasks.

## **Table of Contents**

- [Lab 0: Check the environment](lab0.md)
- [Lab 1: Provision virtual instances on Red Hat OpenStack Platform](lab1.md)
- [Lab 2: Leverage multi-tenancy capabilities of Red Hat OpenStack Platform](lab2.md)
- [Lab 3: Provision virtual instance using Red Hat CloudForms](lab3.md)
- [Lab 4: Configure virtual instance using Red Hat Ansibe Tower](lab4.md)
- [Lab 5: Automate services delivery using Red Hat CloudForms and Red Hat Ansible Tower](lab5.md)
